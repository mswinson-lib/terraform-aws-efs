resource "aws_efs_file_system" "fs" {
  tags {
    Organization = "${var.organization}"
    Name = "${var.fs_name}.${var.zone_name}"
  }
}

resource "aws_security_group" "efs" {
  name = "${var.fs_name}.${var.zone_name}"
  vpc_id = "${var.vpc_id}"
  
  ingress = {
    from_port = "2049"
    to_port = "2049"
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags {
    Organization = "${var.organization}"
    Name = "${var.fs_name}.${var.zone_name}"
  }
}

resource "aws_efs_mount_target" "fsmount" {
  count = "2"

  file_system_id = "${aws_efs_file_system.fs.id}"
  subnet_id = "${var.subnets[count.index]}"
  security_groups = [ "${aws_security_group.efs.id}" ]
}
