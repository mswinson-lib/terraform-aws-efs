variable "organization" {
  type = "string"
  default = "myorg"
}

variable "zone_name" {
  type = "string"
}

variable "subnets" {
  type = "list"
}

variable "vpc_id" {}

variable "fs_name" {
  type = "string"
  default = "myfs"
}

