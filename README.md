# terraform-aws-efs

[Terraform Module]() creates EFS file system

## Features


## Usage

```HCL
module "fs" {
  source = "bitbucket.org/mswinson-lib/terraform-aws-efs"

  zone_name = "<zone name>"  # required
  subnets = [ ]              # required
  vpc_id = "<vpc id>"        # required

  organization = "myorg"     # default
  fs_name = "myfs"           # default
}
```

## Inputs

| Name | Description | Type | Default | Required |
| - | - | - | - | - |
| organization || string | myorg | true |
| zone_name || string || true |
| subnets || list || true |
| vpc_id || string || true |
| fs_name || string | myfs | true |


## Outputs

    None

