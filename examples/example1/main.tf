provider "aws" {
  region = "eu-west-1"
}

variable "organization" {
  default = "tfawsefs"
}

variable "name" {
  default = "example1"
}

variable "zones" {
  default = {
    "zone0" = "eu-west-1a",
    "zone1" = "eu-west-1b"
  }
}

variable "cidr_blocks" {
  default = {
    "zone0" = "10.0.1.0/24",
    "zone1" = "10.0.2.0/24"
  }
}

module "vpc" {
  source = "bitbucket.org/mswinson-lib/terraform-aws-vpc?ref=develop"

  organization = "${var.organization}"
  name = "${var.name}"
}

resource "aws_subnet" "example1" {
  count = 2

  vpc_id = "${module.vpc.vpc}"
  availability_zone = "${lookup(var.zones, "zone${count.index}")}"
  cidr_block = "${lookup(var.cidr_blocks, "zone${count.index}")}"
  map_public_ip_on_launch = true

  tags {
    Name = "${var.name}"
    Organization = "${var.organization}"
  }
}

resource "aws_route_table" "example1" {
  vpc_id = "${module.vpc.vpc}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${module.vpc.igw}"
  }

  tags {
    Name = "${var.name}"
    Organization = "${var.organization}"
  }

}

locals {
  subnets =  [ "${aws_subnet.example1.*.id}" ]
}

resource "aws_route_table_association" "example1" {
  count = 2

  subnet_id = "${element(aws_subnet.example1.*.id, count.index)}"
  route_table_id = "${aws_route_table.example1.id}"
}

module "fs" {
  source = "../.."

  zone_name = "example1"
  subnets =  "${local.subnets}"
  vpc_id = "${module.vpc.vpc}"

  organization = "${var.organization}"
  fs_name = "${var.name}"
}
